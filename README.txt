
Introduction
------------
Provides a page and block, that shows Aurora Borealis activity


Installation
------------
Enable the module in /admin/build/modules
Add the css in aurora_borealis.css to your theme css file
Enable the block


Sources
-------
Aurora description
* http://www.swpc.noaa.gov/ftpdir/latest/RSGA.txt
* readme at http://www.swpc.noaa.gov/ftpdir/forecasts/RSGA/README

Aurora magnetic readings
* http://www.swpc.noaa.gov/ftpdir/lists/ace/ace_mag_1m.txt

Aurora speed and density readings
* http://www.swpc.noaa.gov/ftpdir/lists/ace/ace_swepam_1m.txt

Readme for magnetic, speed and density
* http://www.swpc.noaa.gov/ftpdir/lists/ace/README

