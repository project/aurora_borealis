<?php
/**
 * @file
 * Include file for Aurora Borealis module
 */

/**
 * Get the density and speed from http://swpc.noaa.gov/ftpdir/lists/ace/ace_swepam_1m.txt
 */
function aurora_borealis_speed(&$aurora) {
  $handle = @fopen('http://www.swpc.noaa.gov/ftpdir/lists/ace/ace_swepam_1m.txt', 'r');
  if ($handle) {
  // Show beginning of the hour (+ 7 hours), '2010 07 19  2300'
  $datefind = date('Y m d  Hi', mktime(date(H)+7, 0, 0, date('m'), date('d'), date('Y')));
  $datedb = date('Y-m-d H:00:00', mktime(date(H)+7, 0, 0, date('m'), date('d'), date('Y')));
    while (!feof($handle)) {
      $buffer = fgets($handle, 4096);
      $pos = strpos($buffer, $datefind);
      if ($pos !== FALSE) {
        echo $buffer . '<br/>';
        $buffer = preg_replace('!\s+!', ' ', $buffer);
        list ($y, $m, $d, $time, $day, $sec, $S, $protdens, $blkspeed, $iontemp) = explode(' ', $buffer);
      }
    }
    fclose($handle);

    $aurora->speed = $blkspeed;
    $aurora->density = $protdens;
    $aurora->date = $datedb;
  }
}

/**
 * Get the magnetic density from http://www.swpc.noaa.gov/ftpdir/lists/ace/ace_mag_1m.txt
 */
function aurora_borealis_mag(&$aurora) {
  $handle = @fopen('http://www.swpc.noaa.gov/ftpdir/lists/ace/ace_mag_1m.txt', 'r');
  if ($handle) {
    // Show beginning of the hour (+ 7 hours), '2010 07 19  2300'
    $datefind = date('Y m d  Hi', mktime(date(H)+7, 0, 0, date('m'), date('d'), date('Y')));
    while (!feof($handle)) {
      $buffer = fgets($handle, 4096);
      $pos = strpos($buffer, $datefind);
      if ($pos !== FALSE) {
        echo $buffer . '<br/>';
        $buffer = preg_replace('!\s+!', ' ', $buffer);
        list($y, $m, $d, $time, $day, $sec, $S, $Bx, $By, $Bz, $Bt, $lat, $long) = explode(' ', $buffer);
      }
    }
    fclose($handle);

    $aurora->bt = $Bt;
    $aurora->bz = $Bz;
    $aurora->latitude = $lat;
    $aurora->longitude = $long;
  }
}

/**
 * Get the forecast from http://www.swpc.noaa.gov/ftpdir/latest/RSGA.txt
 */
function aurora_borealis_desc() {
  $handle = @fopen('http://www.swpc.noaa.gov/ftpdir/latest/RSGA.txt', 'r');
  $posiib = FALSE;
  $buffer = '';
  if ($handle) {
    while (!feof($handle)) {
      $buffer = fgets($handle, 4096);
      if ($posiib === FALSE) {
        $posiib = strpos($buffer, 'IIB. ');
      }
      $posiii = strpos($buffer, 'III. ');
      if ($posiib !== FALSE && $posiii !== FALSE) {
        break;
      }
      if ($posiib !== FALSE) {
        $output .= $buffer;
      }
    }
    $desc = substr($output, 6, strlen($output));
    variable_set('aurora_borealis_desc', $desc);
    fclose($handle);
  }
}

/**
 * Calculate the activity, velocity, particle density, magnetic field bars
 */
function aurora_borealis_bars(&$aurora) {
  if ($aurora->speed <= 200) {
    $aurora->barspeed = 1;
  } elseif ($aurora->speed <= 400 && $aurora->speed >= 201) {
    $aurora->barspeed = 2;
  } elseif ($aurora->speed <= 600 && $aurora->speed >= 401) {
    $aurora->barspeed = 3;
  } elseif ($aurora->speed <= 800 && $aurora->speed >= 601) {
    $aurora->barspeed = 4;
  } elseif ($aurora->speed >= 801) {
    $aurora->barspeed = 5;
  }
  if ($aurora->density <= 1) {
    $aurora->bardensity = 1;
  } elseif ($aurora->density >= 2 && $aurora->density <= 5) {
    $aurora->bardensity = 2;
  } elseif ($aurora->density >= 6 && $aurora->density <= 10) {
    $aurora->bardensity = 3;
  } elseif ($aurora->density >= 11 && $aurora->density <= 15) {
    $aurora->bardensity = 4;
  } elseif ($aurora->density >= 16) {
    $aurora->bardensity = 5;
  }
  if ($aurora->bz >= 8) {
    $aurora->barbz = 1;
  } elseif ($aurora->bz >= 3 && $aurora->bz <= 7) {
    $aurora->barbz = 2;
  } elseif ($aurora->bz >= -2 && $aurora->bz <= 2) {
    $aurora->barbz = 3;
  } elseif ($aurora->bz >= -7 && $aurora->bz <= -3) {
    $aurora->barbz = 4;
  } elseif ($aurora->bz <= -8) {
    $aurora->barbz = 5;
  }
  $aurora->activity = round(($aurora->barspeed + $aurora->density + $aurora->bz) / 3);
}
